<p align="center">
    <img src="https://i.imgur.com/cexlqHJ.png" alt="Logo">
</p>

# The Zipper
A command line tool to create a zip file of a given directory. Additionally, a filter is in place to exclude files that are unnecessary for building a typescript project.
## Usage
```node  {Path of index.js} {Path of directory to zip}```

- The subsequent zip file will be created in the same path as the given directory.
- The name of the file with follow the format of `{Name of directory} {Your name} {MONTH}_{DAY}_{YEAR}__{HOUR}_{MINUTE}.zip`

### Example

```node dist/index.js "testing/ZIP THIS ONE"```
```node dist/index.js ../the-zipper```

## Filter

The filter is based off of `.gitignore` and includes:

- `node_modules/`
- `dist/`
- `.env`

If a file is matches or is within the directory of any of these filters, they will be excluded from the zip file.
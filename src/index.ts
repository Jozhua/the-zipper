import { config } from "dotenv";
config();
import AdmZip from "adm-zip";
import fs from "fs";
import path from "path";

const NAME: string = process.env.MYNAME;

// A list of file extensions / directories that are not necessary for building a project or don't want included into the zip.
const filterList: string[] = [
    "node_modules",
    "dist",
    ".env"
];

/**
 * Checks that only one directory, aside from index.js, is provided.
 * @param inputArgs An array of arguments given via the command line.
 * @throws new Error("Incorrect number of arguments received.") if the amount of input arguments is not exactly 3.
 */
function validateInputArgs(inputArgs: string[]): void {
    if (inputArgs.length !== 3) {
        throw new Error("Incorrect number of arguments received.");
    }
}

/**
 * Checks to see if the given directory actually exists.
 * @param normalizedDir The normalized version of the given directory.
 * @throws Error("The directory does not exist.") if it is found the directory does not exist.
 */
function validateDirExists(normalizedDir: string): void {
    if (!fs.existsSync(normalizedDir)) {
        throw new Error("The directory does not exist.");
    }
}

/**
 * Checks if the given directory contains at least one file or folder.
 * @param normalizedDir The normalized version of the given directory.
 * @throws Error("The directory is empty.") if there are no files found within the directory.
 */
function validateDirIsNotEmpty(normalizedDir: string): void {
    // Gives a list of files and folders within the given directory
    if (!fs.readdirSync(normalizedDir).length) {
        throw new Error("The directory is empty.");
    }
}

/**
 * Adds leading zero(s) to the given number to maintain a minimum length.
 * @param origNumber The original number that may or may not require leading zeros to be added.
 * @param minLength The minimum length the final string must be.
 * @returns A string of the given number with leading zeros to reach the minimum length.
 */
function addLeadingZeros(origNumber: number, minLength: number): string {
    return `${origNumber}`.padStart(minLength, "0");
}

/**
 * Decides whether or not an individual file or directory should be included in the zip.
 * @param filePath Path of the folder or file under scrutiny.
 * @returns True if file path should be include else return false because path contains
 * ignored directories.
 */
function shouldFileBeZipped(filePath: string): boolean {
    // If any filterString in the filterList is found within the path of the given filePath, do not include it to be zipped.
    return (filterList.every((filterString: string) => (!filePath.includes(filterString))));
}

function generateZipFileName(filePath: string): string {
    const date: Date = new Date();

    // Add leading zeros to a have a standard filename format of (MM_DD_YYYY__hh__mm).
    const month = addLeadingZeros(date.getMonth() + 1, 2);
    const day = addLeadingZeros(date.getDate(), 2);
    const year = addLeadingZeros(date.getFullYear(), 4);
    const hour = addLeadingZeros(date.getHours(), 2);
    const min = addLeadingZeros(date.getMinutes(), 2);

    return `${filePath} ${NAME} ${month}_${day}_${year}__${hour}_${min}.zip`;
}
/**
 * Zip archive and write to disk all files a given path.
 * @param filePath Path of the folder whose contents will be zipped.
 */
function zipFiles(filePath: string): void {
    const zip: AdmZip = new AdmZip();
    const zippedFilePath: string = generateZipFileName(filePath);

    zip.addLocalFolder(filePath, "", shouldFileBeZipped);
    zip.writeZip(zippedFilePath);

    // eslint-disable-next-line
    console.log(`Successfully zipped file!\n${zippedFilePath}`);
}

/**
 * Validates the input directory for its input, existence and content and then zips the files
 */
function runZip(): void {
    try {
        validateInputArgs(process.argv);

        const normalizedDir: string = path.normalize(process.argv[2]);

        validateDirExists(normalizedDir);
        validateDirIsNotEmpty(normalizedDir);
        zipFiles(normalizedDir);

    } catch (err) {
        // eslint-disable-next-line
        console.error(err.message || err);
    }
}

runZip();